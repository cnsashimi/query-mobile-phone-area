package com.wutong.spboot.mapper;

import com.wutong.spboot.entity.MobileZone;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MobileZoneMapper {
    List<MobileZone> findAll(int limitvar1, int limitvar2);
    List<MobileZone> findOne(int id);
    List<MobileZone> findMap(Map<String, Object> map);
    List<MobileZone> chenckPhone(String Prefix);

    void addSave(Map<String, Object> map);
    void deleteById(int id);
    void updateById(Map<String, Object> map);




}
