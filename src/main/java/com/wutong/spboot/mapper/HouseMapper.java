package com.wutong.spboot.mapper;

import com.wutong.spboot.entity.House;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface HouseMapper {

List<House> findAll();
}
