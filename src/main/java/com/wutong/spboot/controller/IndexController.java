package com.wutong.spboot.controller;

import com.wutong.spboot.entity.CheckCallback;
import com.wutong.spboot.entity.House;
import com.wutong.spboot.entity.MobileZone;
import com.wutong.spboot.service.HouseService;
import com.wutong.spboot.service.MobileZoneService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@RestController
@Controller

public class IndexController {


    @Autowired
    private MobileZoneService mobileZoneService;


    private String prefixPath = "/index/";
    @GetMapping("/")
    public String index(Model model) {

        model.addAttribute("msg", "Hello,wutong");
        model.addAttribute("name", "wutong");
        return prefixPath + "index";
    }

//    @GetMapping({"/index/show/{the_Param}"})
//    public String show(@PathVariable("the_Param") Integer id) {
//
//        System.out.println("这个值为 " + id);
//      return prefixPath + "index";
//    }

    //?/index/show?id=1111
    @GetMapping("/index/show")
    public String show2(Integer id) {
        System.out.println("这个值为 " + id);
        return prefixPath + "index";
    }



    @PostMapping("/index/postsubmit")
    public  String postsubmit(Integer id,String name){

        System.out.println("这个值为id" + id);
        System.out.println("这个name值为" + name);
        return prefixPath + "index";
    }


    @GetMapping("/index2")
    public String index2(Model model) {
            model.addAttribute("msg", "Hello,wutong");
            model.addAttribute("name", "wutong");
            return prefixPath + "index2";

    }





}
