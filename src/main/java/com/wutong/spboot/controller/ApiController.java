package com.wutong.spboot.controller;

import com.wutong.spboot.entity.CheckCallback;
import com.wutong.spboot.entity.MobileZone;
import com.wutong.spboot.service.MobileZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController


public class ApiController {

    @Autowired
    private MobileZoneService mobileZoneService;

    @PostMapping("/api/postcheckphone")
    public CheckCallback postsubmit(long Prefix) {


        String ckPre=String.valueOf(Prefix);



        System.out.println("查询的号是" + Prefix);

        System.out.println(ckPre.length());
    if (ckPre.length()>7){
        System.out.println("大于7");
        System.out.println(ckPre.substring(0,7));
        ckPre= ckPre.substring(0,7);
    }else{

        System.out.println("小于7");
        ckPre= ckPre + "%";
    }

        List<MobileZone> list = mobileZoneService.chenckPhone(ckPre);
        String msg = "";
        int type = 0;
        try {
            long prefix = list.get(0).getPrefix();
            msg = "手机号：[" + list.get(0).getPrefix() + "]来自" + list.get(0).getProvince() + list.get(0).getCity() + "[" + list.get(0).getOperators() + "]";
            type = 1;
        } catch (Exception e) {
            System.out.println(e);
            msg = "手机号：[" + Prefix + "]查不到归属地";
            type = 0;
        }

        return new CheckCallback(msg, type, Prefix);


    }

    @RequestMapping("/mobileapigetall")
    public List<MobileZone> mobileapigetall() {

        return mobileZoneService.findAll(10, 5);
        //return mobileZoneService.findOne(4);
    }

    @RequestMapping("/mobileapigetone")
    public List<MobileZone> mobileapigetone() {

        //   return mobileZoneService.findAll();
        return mobileZoneService.findOne(4);
    }

    @RequestMapping("/mobileapigetmap")
    public List<MobileZone> mobileapigetmap() {
        //  Map<String,String> map = new HashMap<>();  //HashMap 无序不重复无索引  LinkedHashMap 有序不重复无索引 TreeMap 可排序不重复无索引
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("Province", "浙江省");
        map.put("City", "温州市");
        map.put("startIndex", 0);
        map.put("pageSize", 20);

        //   return mobileZoneService.findAll();
        return mobileZoneService.findMap(map);
    }


    //增
    @RequestMapping("/mobileapiadd")
    public String mobileapiadd() {


        Map<String, Object> map = new HashMap<String, Object>();

        map.put("Province", "浙江省");
        map.put("City", "温州市");
        map.put("Prefix", 1300000);
        map.put("Operators", 1300001);

        //   return mobileZoneService.findAll();
        mobileZoneService.addSave(map);
        System.out.println(map);

        return " mobileapiadd ok";
    }

    //删
    @RequestMapping("/mobileapidele")
    public String mobileapidele() {
        mobileZoneService.deleteById(452279);
        return " mobileapidele ok";
    }

    //改

    @RequestMapping("/mobileapiedit")
    public String mobileapiedit() {


        Map<String, Object> map = new HashMap<String, Object>();

        map.put("Province", "浙江省");
        map.put("City", "瑞安市");
        map.put("Prefix", 1300000);
        map.put("Operators", 1300001);
        map.put("id", 452280);

        //   return mobileZoneService.findAll();
        mobileZoneService.updateById(map);
        System.out.println(map);

        return "mobileapiedit ok";
    }



}
