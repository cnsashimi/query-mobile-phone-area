package com.wutong.spboot.controller;


import com.wutong.spboot.entity.CheckCallback;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;


//@ControllerAdvice
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler()
    public CheckCallback exceptionHandle(Exception e){ // 处理方法参数的异常类型
        System.out.println(e);
        //String msg ="出错了：" + e;
        String msg ="请求的数据出错了";

        return new CheckCallback(msg, 0, 0);
    }


}
