package com.wutong.spboot.controller;

import com.wutong.spboot.entity.House;
import com.wutong.spboot.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
private HouseService houseService;

//@RequestMapping("/wutong")
//    public String getName(){
//
//        return "getnameaaaaa";
//    }

    @RequestMapping("/wutong")
    public Animal getName(){

        return new Animal("我",40);
    }

    @RequestMapping("/house")
    public List<House> getHouse(){

        return houseService.findAll();
       // return houseService.findOne(4);
    }
}
