package com.wutong.spboot.entity;

public class CheckCallback {

    private String msg;
    private int type;
    private long Prefix;

    public CheckCallback(String msg, int type, long Prefix) {
        this.msg = msg;
        this.type = type;
        this.Prefix = Prefix;
    }



    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getPrefix() {
        return Prefix;
    }

    public void setPrefix(long Prefix) {
        this.Prefix = Prefix;
    }


}
