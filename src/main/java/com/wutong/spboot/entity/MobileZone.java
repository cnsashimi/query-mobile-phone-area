package com.wutong.spboot.entity;

import java.security.Provider;

public class MobileZone {
    private int id;
    private long Prefix;
    private String Province;
    private String City;
    private String Operators;

    public MobileZone(int id, int prefix, String province, String city, String operators) {
        this.id = id;
        Prefix = prefix;
        Province = province;
        City = city;
        Operators = operators;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPrefix() {
        return Prefix;
    }

    public void setPrefix(long prefix) {
        Prefix = prefix;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getOperators() {
        return Operators;
    }

    public void setOperators(String operators) {
        Operators = operators;
    }
}
