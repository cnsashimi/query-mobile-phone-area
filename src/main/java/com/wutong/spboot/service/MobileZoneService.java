package com.wutong.spboot.service;

import com.wutong.spboot.entity.MobileZone;
import com.wutong.spboot.mapper.MobileZoneMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class MobileZoneService {
    @Autowired
    private MobileZoneMapper mobileZoneMapper;

    public List<MobileZone> findAll(int limitvar1, int limitvar2) {
        return mobileZoneMapper.findAll(limitvar1, limitvar2);
    }

    public List<MobileZone> findOne(int id) {
        return mobileZoneMapper.findOne(id);
    }
    public List<MobileZone> chenckPhone(String Prefix) {
        return mobileZoneMapper.chenckPhone(Prefix);
    }

    public List<MobileZone> findMap(Map<String, Object> map) {
        return mobileZoneMapper.findMap(map);
    }

    public void addSave(Map<String, Object> map) {
         mobileZoneMapper.addSave(map);
    }

    public void updateById(Map<String, Object> map) {
        mobileZoneMapper.updateById(map);
    }

    public void deleteById(int id) {
       mobileZoneMapper.deleteById(id);
    }


}
