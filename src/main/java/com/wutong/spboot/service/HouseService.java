package com.wutong.spboot.service;

import com.wutong.spboot.entity.House;
import com.wutong.spboot.mapper.HouseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class HouseService {

    @Autowired
    private HouseMapper houseMapper;
    public List<House> findAll(){

    return houseMapper.findAll();
    }
}
