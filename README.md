# java查询手机归属地,练手笔记 

学习目标： 熟悉springBoot结构  MyBatis使用方法 



## 1.新建项目 for idea

### spring Initializr

- Spring Boot DevTools
- Spring Web
- MySQL Driver 或 SQLITE(二选一)
- MyBatis Framework

## 2.初始自己的项目

### 1.建立Controller

- IndexController

  @RestController
  
  public class IndexController {
      @Autowired
      @RequestMapping("/")
      public String index(){
          return "你好 ";
      }
  ｝
  
  
### 2.设置SrpingBoot的配置文件 application.yml

正文内容 ：

server:  
  port: 8090  //配置端口
  session-timeout: 30  
  tomcat.max-threads: 0  
  tomcat.uri-encoding: UTF-8  
  
spring:  
  datasource:  //数据库配置
    url : jdbc:mysql://localhost:3306/newbirds  
    username : root  
    password : mymysql  
    driverClassName : com.mysql.jdbc.Driver  

age: 18
name: jason
manInfo:
	age: 18
	name: jason


//如何获取（利用)这些变量:  在class里 使用 

// @Value("${name}")
//  private String name;



- 设置好本地Mysql地址用户名密码
- 具体内容：

  server:
    port: 8000
  spring:
    datasource:
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
      username: root
      password: root
  
  mybatis:
    mapper-locations: classpath:mapper/*.xml
  
  
  
### 3.给数据表建一个实体入口

- Mysql数据库表mobilezone结构

  CREATE TABLE `mobilezone` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `Prefix` int(8) DEFAULT NULL,
    `Province` varchar(100) DEFAULT NULL,
    `City` varchar(100) DEFAULT NULL,
    `Operators` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `Prefix` (`Prefix`)
  ) ENGINE=InnoDB AUTO_INCREMENT=452277 DEFAULT CHARSET=utf8mb4;
  
  数据表见我仓库 https://gitee.com/cnsashimi/mobileregion  
  
  
	- 建立entity 软件包 下属建立MobileZone.java

	  public class MobileZone {
	      private int id;
	      private int Prefix;
	      private String Province;
	      private String City;
	      private String Operators;
	  }
	  定义好和数据表结构一样的字段 按 Alt+Ins
	  生成构造函数 全选所有字段
	  及 生成 Get和Set 方法
	  
### 4.给数据表建一个映射命令接口

- 建立mapper软件包 下属建立 MobileZoneMapper.java

  @Mapper
  public interface MobileZoneMapper {
      List<MobileZone> findAll();
  }
  
  
- 具体的映射SQL命令书写到resources/mapper 目录里（可在application.yml里修改）

	- MobileZoneMapper.xml 

	  <?xml version="1.0" encoding="UTF-8" ?>
	  <!DOCTYPE mapper    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
	  <mapper namespace="com.wutong.spboot.mapper.MobileZoneMapper">
	      <select id="findAll" resultType="com.wutong.spboot.entity.MobileZone">
	          select * from mobilezone.mobilezone limit 10
	      </select>
	  </mapper>
	  
	  
### 5.给数据表建一个服务

- 建立service软件包 下属建立 MobileZoneService.java

  @Mapper
  public interface MobileZoneMapper {
      List<MobileZone> findAll();
  }
  
## 3.Controller控制器里调用数据表内容

### 1.json输出表内数据

建立一个controller  

@RequestMapping("/mobileapi")
public List<MobileZone> mobileapi(){
    return mobileZoneService.findAll();
}
运行SpbootApplication 后 浏览器输入    127.0.0.1:8000/mobileapi    即可输出json格式表内数据

### 2.使用集合做SQL复合检索输出

- Mapper.xml

  <select id="findMap" parameterType="map" resultType="com.wutong.spboot.entity.MobileZone">
      select * from mobilezone.mobilezone where Province like #{Province} and City like #{City} limit #{startIndex},#{pageSize}
  </select>
  
  
- 程序内用法

  mapper.java
  List<MobileZone> findMap(Map<String, Object> map);
  service.java
  public List<MobileZone> findMap(Map<String, Object> map){
      return mobileZoneMapper.findMap(map);
  }
  
  
  Controller.java
  @RequestMapping("/mobileapigetmap")
  public List<MobileZone> mobileapigetmap(){
    //HashMap 无序不重复无索引  LinkedHashMap 有序不重复无索引 TreeMap 可排序不重复无索引
      Map<String, Object> map = new HashMap<String, Object>();
  
      map.put("Province","浙江省");
      map.put("City","温州市");
      map.put("startIndex",0);
      map.put("pageSize",20);
  
   //   return mobileZoneService.findAll();
      return mobileZoneService.findMap(map);
  }
  
### 3.增删改

- Mapper.xml

  <!--增 用临时表做测试 增删改 不需要 resultType  -->
      <insert id="addSave" parameterType="map" >
          INSERT INTO mobilezone.mobilezone_temp(`Prefix`,`Province`,`City`,`Operators`) VALUES (#{Prefix},#{Province},#{City},#{Operators});
      </insert>
  
      <!--删 用临时表做测试   增删改 不需要 resultType -->
  <delete  id="deleteById" parameterType="int" >
      delete from mobilezone.mobilezone_temp where id=#{id}
  </delete>
  
  
      <!--改 用临时表做测试 增删改 不需要 resultType  -->
  
  <update id="updateById" parameterType="map" >
      update mobilezone.mobilezone_temp set
      Prefix= #{Prefix},Province= #{Province},City= #{City},Operators= #{Operators}
      where id = #{id}
  </update>
  
  
- 程序内用法

  Mapper.java
      void addSave(Map<String, Object> map);
      void deleteById(int id);
      void updateById(Map<String, Object> map);
  
  
  
  Service.java
      public void addSave(Map<String, Object> map) {
           mobileZoneMapper.addSave(map);
      }
  
      public void updateById(Map<String, Object> map) {
          mobileZoneMapper.updateById(map);
      }
  
      public void deleteById(int id) {
         mobileZoneMapper.deleteById(id);
      }
  
  
  Controller.java
      //增
      @RequestMapping("/mobileapiadd")
      public String mobileapiadd() {
  
  
          Map<String, Object> map = new HashMap<String, Object>();
  
          map.put("Province", "浙江省");
          map.put("City", "温州市");
          map.put("Prefix", 1300000);
          map.put("Operators", 1300001);
  
          //   return mobileZoneService.findAll();
          mobileZoneService.addSave(map);
          System.out.println(map);
  
          return " mobileapiadd ok";
      }
  
      //删
      @RequestMapping("/mobileapidele")
      public String mobileapidele() {
          mobileZoneService.deleteById(452279);
          return " mobileapidele ok";
      }
  
      //改
  
      @RequestMapping("/mobileapiedit")
      public String mobileapiedit() {
  
  
          Map<String, Object> map = new HashMap<String, Object>();
  
          map.put("Province", "浙江省");
          map.put("City", "瑞安市");
          map.put("Prefix", 1300000);
          map.put("Operators", 1300001);
          map.put("id", 452280);
  
          //   return mobileZoneService.findAll();
          mobileZoneService.updateById(map);
          System.out.println(map);
  
          return "mobileapiedit ok";
      }
  
## 4.项目目录结构说明

<div>
  ├─main<br />
  │  ├─java<br />
  │  │  ├─com<br />
  │  │  │  └─wutong<br />
  │  │  │      └─spboot<br />
  │  │  │          │  SpbootApplication.java	#启动入口<br />
  │  │  │          │<br />
  │  │  │          ├─controller			#控制器程序存放目录 （文件名可以 AbcController.java形式 当然也可以乱取）<br />
  │  │  │          │<br />
  │  │  │          ├─entity			#数据表实体化入口文件目录 （文件名可以以 表名.java形式  当然也可以乱取）<br />
  │  │  │          │<br />
  │  │  │          ├─mapper			#数据表命令接口文件目录 (文件名可以以 表名Mapper.java形式  当然也可以乱取）<br />
  │  │  │          │<br />
  │  │  │          └─service			#数据表服务文件目录 (文件名可以以  表名Service.java形式  当然也可以乱取）<br />
  │  │  │<br />
  │  │  └─META-INF<br />
  │  │<br />
  │  └─resources					#资源目录<br />
  │      │  application.yml			#springBoot 设置文件<br />
  │      │<br />
  │      ├─mapper					#表接口SQL执行命令xml文件(文件名可以以 表名Mapper.xml形式  当然也可以乱取）<br />
  │      │<br />
  │      ├─META-INF<br />
  │      │      MANIFEST.MF<br />
  │      │<br />
  │      ├─static					#WEB对外访问的根目录<br />
  │      └─templates				#模板目录<br />
  └─test
  
  
  </div>

## 5.SpringBoot thymeleaf 

### 1.导入thymeleaf包

因为新建项目时没选thymeleaf 
修改pom.xml 增加 重载maven
<!--thymeleaf 模板-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>

### 2.在 resources/templates/存放html模板文件

最好要在 resources/templates/XXXX   的子目录里存在  直接放在 templates根目录下 我自己调试起来多多少少会因为权限及安全问题出现一些bug 比如渲染失败 th斌值失败等情况

html标准格式:
	
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <title>hello world</title>
</head>
<body>
<h1>hello</h1>
<p>world</p>
<p th:text="${name}"></p>
<hr>
<div th:text="${msg}"></div>
</body>
</html>

### 3.GET方式调用模板

Controller.java

private String prefixPath = "/index/";

	//get 访问 无传参
	@GetMapping("/index")
public String index(Model model) {
    model.addAttribute("msg", "Hello,wutong");
    model.addAttribute("name", "wutong");
return prefixPath + "index";//对应模板目录里 templates/index/index.html
}



//get 有传参
    @GetMapping({"/index/show/{the_Param}"})
   public String show(Model model,@PathVariable("the_Param") Integer id) {
   System.out.println("这个值为 " + id);
return prefixPath + "index";//对应模板目录里 templates/index/index.html
｝


//  ?传参     /index/show?id=1111
@GetMapping("/index/show")
public String show2(Integer id) {
    System.out.println("这个值为 " + id);
    return prefixPath + "index";
}

### 4.POST方式

@PostMapping("/index/postsubmit")
public  String postsubmit(Integer id,String name){

    System.out.println("这个值为id" + id);
    System.out.println("这个name值为" + name);
    return prefixPath + "index";
}


### 5.定义错误页、容错处理

## 6.开始前端

### 前端 layui 写完 

### 数据容错判断 

- 全局异常捕获GlobalExceptionHandler

  在controller 新建一个 GlobalExceptionHandler类
  
  内容：
  //@ControllerAdvice
  @RestControllerAdvice
  public class GlobalExceptionHandler {
  
      @ExceptionHandler()
      public CheckCallback exceptionHandle(Exception e){ // 处理方法参数的异常类型
          System.out.println(e);
          //String msg ="出错了：" + e;
          String msg ="请求的数据出错了";
          return new CheckCallback(msg, 0, 0);
      }
  }
  
- mapper.xml查询语句修改成like %

  <select id="chenckPhone"   parameterType="String" resultType="com.wutong.spboot.entity.MobileZone">
  
      select * from mobilezone.mobilezone where Prefix like  #{Prefix,jdbcType=VARCHAR}"%" limit 1
  </select>
  
  
- Post过来手机号从原来int转成String并做长度判断补位或删位

  @PostMapping("/api/postcheckphone")
  public CheckCallback postsubmit(int Prefix) {
  
  
      String ckPre=String.valueOf(Prefix);
  
  
  
      System.out.println("查询的号是" + Prefix);
  
      System.out.println(ckPre.length());
  if (ckPre.length()>7){
      System.out.println("大于7");
      System.out.println(ckPre.substring(0,7));
      ckPre= ckPre.substring(0,7);
  }else{
      System.out.println("小于7");
  
  }
  
      List<MobileZone> list = mobileZoneService.chenckPhone(ckPre);
      String msg = "";
      int type = 0;
      try {
          int prefix = list.get(0).getPrefix();
          msg = "手机号：[" + list.get(0).getPrefix() + "]来自" + list.get(0).getProvince() + list.get(0).getCity() + "[" + list.get(0).getOperators() + "]";
          type = 1;
      } catch (Exception e) {
          System.out.println(e);
          msg = "手机号：[" + Prefix + "]查不到归属地";
          type = 0;
      }
  
      return new CheckCallback(msg, type, Prefix);
  
  
  }
  
  
## 7.番外:  数据库改 SQLITE 

### pom.xml修改

原基础增加
<dependency>
    <groupId>org.xerial</groupId>
    <artifactId>sqlite-jdbc</artifactId>
    <version>3.42.0.0</version>
</dependency>

保证
原来的还有spring-boot-starter-jdbc


### application.yml修改

注解掉原mysql相关设置

增加


spring:
  datasource:
#SQLITE
    url: jdbc:sqlite:H:/javapro/querymobileareasqlite/sqlitedb.db
    driver-class-name: org.sqlite.JDBC
    username: sa
    password: sa
    hikari:
      maximum-pool-size: 1
 sql:
    init:
      mode: never



### Mapper.xml  like相关sql修改（详见具程序）

